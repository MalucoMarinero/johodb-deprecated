(function() {
  'use strict';  $(document).ready(function() {
    var heights, nav, sections, selectNavLi, updateLinks, updateScrollHeights;

    nav = $('nav ul.nav');
    sections = $('section.demonstration section');
    selectNavLi = function(name) {
      nav.find('li').removeClass('active').removeClass('next');
      nav.find("[href=#" + name + "]").parent().addClass('active');
      return nav.find("[href=#" + name + "]").parent().next().addClass('next');
    };
    selectNavLi("introduction");
    heights = [];
    updateScrollHeights = function() {
      heights = [];
      return sections.each(function(i, elem) {
        return heights.unshift([$(elem).offset().top, elem.id]);
      });
    };
    updateLinks = function() {
      var height, scroll, _i, _len, _results;

      scroll = $(document).scrollTop() + 25;
      _results = [];
      for (_i = 0, _len = heights.length; _i < _len; _i++) {
        height = heights[_i];
        if (scroll > height[0]) {
          selectNavLi(height[1]);
          break;
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    updateScrollHeights();
    updateLinks();
    $(document).scroll(updateLinks);
    return $(document).resize(updateScrollHeights);
  });

}).call(this);
