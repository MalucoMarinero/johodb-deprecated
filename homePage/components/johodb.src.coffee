'use strict'

class window.JohoDB
  constructor: (@name, @version) ->
    @rawSchema = {}

  addSchema: (schemaName, schema) ->
    @rawSchema[schemaName] = schema

  initDB: (storageType, opts) ->
    @__buildJSSchema()
    if storageType == "indexedDb"
      @store = new JohoDB.IndexedDbStorage @name, @version
      model.store = @store for key, model of @models
    else if storageType == "webSql"
      @store = new JohoDB.WebSqlStorage @name, @version
      model.store = @store for key, model of @models

    @store.initDB {models: @models, relationTables: @relationTables}, opts

  __buildJSSchema: ->
    @models = {}
    for own modelName, modelSchema of @rawSchema
      @models[modelName] = new JohoDB.Model modelName, modelSchema
    @__setupRelations()

  __setupRelations: ->
    @relationTables = {}
    @relations = []
    for own modelName, model of @models
      @relations = @relations.concat model.getRelations()
    @__linkPrimaryKeys()
    @__formRelations()

  __linkPrimaryKeys: ->
    for r in @relations
      r.from.primaryKey = @models[r.from.modelName].primaryKey
      r.to.primaryKey = @models[r.to.modelName].primaryKey

  __formRelations: ->
    for r in @relations
      if r.requiresRelationTable
        @relationTables[r.getTableName()] = r.buildRelationTable()
      [from, to] = r.buildRelationManagers() 
      @models[from.ownModel].relations[from.name] = from
      @models[from.ownModel].relations[from.name].model = @models[to.ownModel]
      @models[to.ownModel].relations[to.name] = to
      @models[to.ownModel].relations[to.name].model = @models[from.ownModel]
      

    
  


class window.JohoDB.Field
  constructor: (@name, traits, skipCheck) ->
    if not skipCheck
      if traits.type.match /(int|integer)/
        traits.type = "Int"
        return new JohoDB.IntField(@name, traits, true)
      if traits.type.match /(str|string|text)/
        traits.type = "String"
        return new JohoDB.TextField(@name, traits, true)
      if traits.type.match /(datetime)/
        traits.type = "DateTime"
        return new JohoDB.DateTimeField(@name, traits, true)
      if traits.type.match /(fk|foreignKey|foreignkey)/
        traits.type = "ForeignKey"
        return new JohoDB.ForeignKeyField(@name, traits, true)
      if traits.type.match /(m2m|manytomany|manyToMany)/
        traits.type = "ManyToMany"
        return new JohoDB.ManyToManyField(@name, traits, true)
    for own key, value of traits
      @[key] = value

  defaultValidators: []
  validate: (value) ->
    if @required
      if value == undefined
        console.error "Validation Error for #{ @name }, required."
    if @primaryKey
      if not @autoIncrement and not @autoHash
        if value == undefined
          console.error "Validation Error for #{ @name }, required primary key / no auto."
    for validator in @defaultValidators
      if not validator(value)
        console.error "Validation Error for #{ @name }"
    if @validators
      for validator in @validators
        if not validator(value)
          console.error "Validation Error for #{ @name }"
    return true
    

class window.JohoDB.IntField extends JohoDB.Field
  defaultValidators: [
    (value) ->
      number = parseInt value
      if number == NaN
        return false
      return true
  ]

class window.JohoDB.TextField extends JohoDB.Field

class window.JohoDB.DateTimeField extends JohoDB.Field

class window.JohoDB.RelationField extends JohoDB.Field

class window.JohoDB.ForeignKeyField extends JohoDB.RelationField
  validate: -> true

class window.JohoDB.ManyToManyField extends JohoDB.RelationField
  validate: -> true



'use strict'

class window.JohoDB.Model
  constructor: (@name, @rawSchema) ->
    @fields = {}
    @relations = {}
    for own fieldName, fieldTraits of @rawSchema
      @fields[fieldName] = new JohoDB.Field(fieldName, fieldTraits)
    @primaryKey = @getPrimaryKey().name

  getRelations: ->
    relations = []
    for own fieldName, field of @fields
      if field.type.match /(ForeignKey|ManyToMany)/
        relations.push new JohoDB.Relation(@name, field)
    return relations

  getPrimaryKey: ->
    for fieldName, field of @fields
      if field.primaryKey
        return field
    throw "No Primary Key found for #{ @name }"

  validate: (obj) ->
    for own key, field of @fields
      field.validate(obj[key])
    for own key, relation of @relations
      relation.validate(obj[key])

  __reduceObj: (obj) ->
    saveObj = {}
    for own key, __ of @fields
      saveObj[key] = obj[key]
    for own key, relation of @relations
      if relation.type == "ForeignKey"
        saveObj[key] = relation.reduce obj[key]
      if relation.type == "ManyToMany"
        delete saveObj[key]
      if relation.type == "OneToMany"
        delete saveObj[key]
    return saveObj

  __collectRelatedObjectUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      updates[r.relatedModel] = {model: r.model, objs: []}
    for own key, r of @relations
      newObjs = r.collectRelatedObjects obj[key], obj[@primaryKey]
      updates[r.relatedModel].objs = updates[r.relatedModel].objs.concat newObjs
    return updates

  __collectForeignKeyUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      updates[r.name] = {model: r.model, name: r.relatedName, ids: []}
    for own key, r of @relations
      if r.type == "OneToMany"
        updates[r.name].ids = updates[r.name].ids.concat r.collectKeys obj[key]
    return updates

  __collectManyToManyUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      if r.type == "ManyToMany"
        updates[r.relationTableName] = {name: r.relationTableName, updates: [], ownField: r.ownModel + '_' + r.ownKey}
    for own key, r of @relations
      if r.type == "ManyToMany"
        updates[r.relationTableName].updates = updates[r.relationTableName].updates.concat r.collectKeys(obj[key], obj[@primaryKey])
    return updates

  generatePrimaryKey: (obj) ->
    string = JSON.stringify obj
    return CryptoJS.SHA1(string + new Date().getTime()).toString()

  __updateForeignKeyRelation: (objID, fieldName, reverseID) ->
    deferred = Q.defer()
    p = @store.updateForeignKey(@, objID, fieldName, reverseID)
    p.then (result) ->
      deferred.resolve result
    deferred.promise

  __updateRelations: (extraUpdates, keyUpdates, m2mUpdates, reverseID) ->
    stack = []
    if extraUpdates
      for modelName, updateSet of extraUpdates
        for rel in updateSet.objs
          stack.push updateSet.model.save rel
    if keyUpdates
      for fieldName, updateSet of keyUpdates
        for relID in updateSet.ids
          stack.push updateSet.model.__updateForeignKeyRelation(relID, updateSet.name, reverseID)
    if m2mUpdates
      for tableName, updateSet of m2mUpdates
        stack.push @store.clearManyToManyRelations tableName, reverseID, updateSet.ownField
        stack.push @store.createManyToManyRelations tableName, updateSet.updates
    g = Q.all stack

  __saveRelatedObjects: (extraUpdates) ->
    stack = []
    return g

  
  delete: (obj, opts) ->
    deferred = Q.defer()
    if obj[@primaryKey]
      query = {}
      query[@primaryKey] = obj[@primaryKey]
      @query().get(query).evaluate().then (trueObj) =>
        stack = []
        for own key, r of @relations
          if r.type == "OneToMany"
            if r.onDelete == 'cascade' and trueObj[key] != undefined
              for relObj in trueObj[key]
                stack.push r.model.delete(relObj)
            else if r.onDelete == 'setNull' and trueObj[key] != undefined
              for relObj in trueObj[key]
                relObj[r.relatedName] = undefined
                stack.push r.model.save(relObj)
          if r.type == "ManyToMany"
            stack.push @store.clearManyToManyRelations(r.relationTableName, trueObj[@primaryKey], r.ownModel + '_' + r.ownKey)
        stack.push @store.delete(this, trueObj[@primaryKey], {})
        g = Q.all stack
        g.then ->
          deferred.resolve true
    else
      console.error "Object was not provided with a primary key", obj
      deferred.reject "Error"
    deferred.promise


  save: (obj, opts) ->
    deferred = Q.defer()
    extraUpdates = null
    keyUpdates = null
    if @validate obj
      if obj[@primaryKey] == undefined
        obj[@primaryKey] = @generatePrimaryKey obj
      saveObj = @__reduceObj obj
      if opts?.updateRelatedObjs
        extraUpdates = @__collectRelatedObjectUpdates obj
        m2mUpdates = @__collectManyToManyUpdates obj
      if not opts?.updateRelatedObjs and opts?.updateRelationships
        keyUpdates = @__collectForeignKeyUpdates obj
        m2mUpdates = @__collectManyToManyUpdates obj
      p = @store.save(@, saveObj, opts)
      p = p.then (result) =>
        if extraUpdates or keyUpdates or m2mUpdates
          g = @__updateRelations extraUpdates, keyUpdates, m2mUpdates, obj[@primaryKey]
          g.then (relatedObjs) =>
            console.log "Saved #{ @name } with Relations", saveObj, relatedObjs
            deferred.resolve result, relatedObjs
        else if keyUpdates
          g = @__updateForeignKeyRelations keyUpdates, obj[@primaryKey]
          g.then (relatedObjs) =>
            console.log "Saved #{ @name } and updated Keys on Relations", saveObj, relatedObjs
            deferred.resolve result, relatedObjs
        else
          console.log "Saved #{ @name }", saveObj
          deferred.resolve result
      p.fail (e) ->
        deferred.reject e
      p.done()
    deferred.promise

  query: -> new JohoDB.Queryset this


class window.JohoDB.Queryset
  constructor: (@model) ->
    @querySteps = []

  all: ->
    @querySteps.push {
      action: 'all'
      args: {}
    }
    this

  get: (args) ->
    newStep = {
      action: 'get'
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }
      
    if newStep.args.length < 1
      console.error "No arguments provided for get", args
      throw "No arguments provided for get"
    @querySteps.push newStep

    this

  filter: (args) ->
    newStep = {
      action: "filter"
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }

    if newStep.args.length < 1
      console.error "No arguments provided for filter", args
      throw "No arguments provided for filter"

    @querySteps.push newStep
    this

  exclude: (args) ->
    newStep = {
      action: "exclude"
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }

    if newStep.args.length < 1
      console.error "No arguments provided for filter", args
      throw "No arguments provided for filter"

    @querySteps.push newStep
    this

  hasStep: (stepName) ->
    for step in @querySteps
      if step.action == stepName
        return true
    return false

  getStep: (stepName) ->
    for step in @querySteps
      if step.action == stepName
        return step
    return null

  evaluate: (opts) ->
    if not opts then opts = {}
    if opts.depth == undefined then opts.depth = 1
    deferred = Q.defer()
    p = @model.store.evaluateQuery @model, this, opts
    p = p.then (result) =>
      if opts?.depth
        if Array.isArray result
          stack = []
          for obj in result
            stack.push @__getRelatedObjects(obj, {depth: opts.depth - 1})
          g = Q.all stack
        else
          g = @__getRelatedObjects result, {depth: opts.depth - 1}
        g.then (relatedObjs) =>
          @__zipRelatedObjs result, relatedObjs
          deferred.resolve result
      else
        deferred.resolve result
    p.fail (e) ->
      deferred.reject e
    p.done()
    deferred.promise

  __getRelatedObjects: (obj, opts) ->
    stack = []
    for name, relation of @model.relations
      if relation.type == 'ForeignKey' and obj[name] != undefined
        query = {}
        query[relation.relatedKey] = obj[name]
        stack.push relation.model.query().get(query).evaluate(opts)
      else if relation.type == 'OneToMany'
        query = {}
        query[relation.relatedName] = obj[@model.primaryKey]
        stack.push relation.model.query().filter(query).evaluate(opts)
      else if relation.type == 'ManyToMany'
        stack.push @model.store.collectManyToManyRelations(relation, obj[@model.primaryKey])
      else
        func = -> return []
        stack.push func()
    Q.all stack

  __zipRelatedObjs: (result, relatedObjs) ->
    if Array.isArray result
      i = 0
      for res in result
        @__zipRelatedObj res, relatedObjs[i]
        i += 1
    else
      @__zipRelatedObj result, relatedObjs

  __zipRelatedObj: (obj, relatedObjs) ->
    i = 0
    for name, relation of @model.relations
      obj[name] = relatedObjs[i]
      i += 1






class window.JohoDB.FieldLookup
  constructor: (name, value) ->
    [field, lookupType] = name.split '__'
    @field = field
    @type = lookupType or 'exact'
    @value = value

'use strict'

class window.JohoDB.Relation
  constructor: (fromModelName, fromField, skipCheck) ->
    if not skipCheck
      if fromField.type == "ForeignKey"
        return new JohoDB.ForeignKeyRelation fromModelName, fromField, true
      if fromField.type == "ManyToMany"
        return new JohoDB.ManyToManyRelation fromModelName, fromField, true
    @type = fromField.type
    @from =
      modelName: fromModelName
      fieldName: fromField.name
    @to =
      modelName: fromField.relation
      fieldName: fromField.relatedName
    if fromField.onDelete
      @onDelete = fromField.onDelete
    else
      @onDelete = "cascade"

  buildRelationManagers: -> throw "buildRelationManagers Not Implemented"
  buildRelationTable: -> throw "buildRelationTable Not Implemented"
  getTableName: -> throw "getTableName Not Implemented"

class window.JohoDB.ForeignKeyRelation extends JohoDB.Relation
  requiresRelationTable: false

  buildRelationManagers: ->
    fromManager = new JohoDB.ForeignKeyRelationManager(
      @from.fieldName, @to.fieldName, null, @from.modelName, @to.modelName, @to.primaryKey, @from.primaryKey, null)
    toManager = new JohoDB.OneToManyRelationManager(
      @to.fieldName, @from.fieldName, null, @to.modelName, @from.modelName, @from.primaryKey, @to.primaryKey, @onDelete)

    return [fromManager, toManager]


class window.JohoDB.ManyToManyRelation extends JohoDB.Relation
  requiresRelationTable: true

  buildRelationManagers: ->
    fromManager = new JohoDB.ManyToManyRelationManager(
      @from.fieldName, @to.fieldName, @getTableName(), @from.modelName, @to.modelName, @to.primaryKey, @from.primaryKey, null)
    toManager = new JohoDB.ManyToManyRelationManager(
      @to.fieldName, @from.fieldName, @getTableName(), @to.modelName, @from.modelName, @from.primaryKey, @to.primaryKey, null)

    return [fromManager, toManager]

  getTableName: ->
    return "#{ @from.modelName }_#{ @from.fieldName }__#{ @to.modelName }_#{ @to.fieldName }"
  
  buildRelationTable: ->
    return new JohoDB.RelationTable @getTableName(), @from, @to


class window.JohoDB.RelationTable
  constructor: (@name, from, to) ->
    @fields = {}

    @fields["id"] = new JohoDB.Field "id", type: "string", primaryKey: true, autoHash: true
    @primaryKey = "id"

    fromName = "#{ from.modelName }_#{ from.primaryKey }"
    @fields[fromName] = new JohoDB.Field fromName, type: "int", required: true

    toName = "#{ to.modelName }_#{ to.primaryKey }"
    @fields[toName] = new JohoDB.Field toName, type: "int", required: true



class window.JohoDB.ModelRelationManager
  constructor: (@name, @relatedName, @relationTableName, @ownModel, @relatedModel, @relatedKey, @ownKey, @onDelete) ->

  validate: (value) ->
    console.error "Validate not implemented."

    


class window.JohoDB.ForeignKeyRelationManager extends JohoDB.ModelRelationManager
  type: 'ForeignKey'

  validate: (value) ->
    if typeof value == "object"
      if value[@relatedKey] == undefined
        console.error "No primary key provided for relation #{ @name }", value
        throw "Validation Eror"
    if Array.isArray value
      console.error "Foreign Key #{ @name } doesn't support arrays.", value
      throw "Validation Eror"

  reduce: (value) ->
    if typeof value == "object"
      return value[@relatedKey]
    else
      return value

  collectRelatedObjects: (value, reversePk) ->
    if typeof value == "object"
      return [value]
    else
      return []

class window.JohoDB.OneToManyRelationManager extends JohoDB.ModelRelationManager
  type: 'OneToMany'

  validate: (value) ->
    if Array.isArray value
      for rel in value
        if typeof value == "object"
          if rel[@relatedKey] == undefined
            console.error "No primary key provided for relation #{ @name }", value
            throw "Validation Eror"

  collectRelatedObjects: (value, reversePk) ->
    if Array.isArray value
      for obj in value
        obj[@relatedName] = reversePk
      return value
    else
      return []

  collectKeys: (value) ->
    keys = []
    if Array.isArray value
      for obj in value
        keys.push obj[@model.primaryKey]
    return keys

class window.JohoDB.ManyToManyRelationManager extends JohoDB.ModelRelationManager
  type: 'ManyToMany'

  validate: (value) ->
    if Array.isArray value
      for rel in value
        if typeof value == "object"
          if rel[@relatedKey] == undefined
            console.error "No primary key provided for relation #{ @name }", value
            throw "Validation Eror"

  collectRelatedObjects: (value, reversePk) ->
    if Array.isArray value
      return value
    else
      return []

  collectKeys: (value, reversePk) ->
    rels = []
    if Array.isArray value
      for obj in value
        relObj = {}
        relObj[@ownModel + '_' + @ownKey] = reversePk
        relObj[@relatedModel + '_' + @relatedKey] = obj[@model.primaryKey]
        rels.push relObj
    return rels



'use strict'

class window.JohoDB.BaseStorage
  constructor: (@name, @version) ->

  initDB: (jsSchema) ->
    throw "initDB Not implemented"

  modelMethods:
    save: (model, obj, opts) ->
      throw "save not implemented"

      

if window.webkitIndexedDB
  window.indexedDB = window.webkitIndexedDB
  window.IDBKeyRange = window.webkitIDBKeyRange
  window.IDBTransaction = window.webkitIDBTransaction

READ_WRITE_FLAG = "readwrite"


__deleteCursorPromise = (cursor) ->
  deferred = Q.defer()
  dreq = cursor.delete()
  dreq.onsuccess = (e) ->
    deferred.resolve true
  deferred.promise


class window.JohoDB.IndexedDbStorage extends JohoDB.BaseStorage
  initDB: (jsSchema, initOpts) ->
    dbDefer = Q.defer()
    @promise = dbDefer.promise
    @p = dbDefer.promise

    clobber = Q.defer()
    if initOpts.clobber
      req = window.indexedDB.deleteDatabase @name
      req.onsuccess = (e) =>
        clobber.resolve true
        console.warn "Clobbered Database #{ @name }"
      req.onerror = (e) ->
        clobber.reject e
        throw e
    else
      clobber.resolve true
    clobber.promise.then (result) => @__startDB jsSchema, initOpts, dbDefer

  __startDB: (jsSchema, initOpts, dbDefer) ->

    req = window.indexedDB.open @name, @version
    req.onsuccess = (e) =>
      idb = e.target.result
      console.log 'Finished loading', @
      dbDefer.resolve idb

    req.onupgradeneeded = (e) =>
      idb = e.target.result
      console.log 'Upgrading'
      @__buildTables idb, jsSchema, initOpts, e
    req.onerror = (e) ->
      dbDefer.reject e
      throw e


  __buildTables: (idb, jsSchema, initOpts, e) ->
    for modelName, model of jsSchema.models
      @__buildTable idb, model
    for relationName, relationTable of jsSchema.relationTables
      @__buildTable idb, relationTable


  __buildTable: (idb, model) ->
    store = idb.createObjectStore model.name, {keyPath: model.primaryKey, autoIncrement: true}
    field.makeIDBIndex(store) for name, field of model.fields


  evaluateQuery: (model, queryset, opts) ->
    deferred = Q.defer()

    @promise.then (idb) =>
      if queryset.hasStep "get"
        lookups = queryset.getStep("get").args
        @__get(model, idb, lookups, opts).then (result) ->
          deferred.resolve result
      else if queryset.hasStep "all"
        @__getAll(model, idb, opts).then (result) ->
          deferred.resolve result
      else
        @__executeQuery(model, idb, queryset.querySteps, opts).then (result) ->
          deferred.resolve result

    deferred.promise
    



  __executeQuery: (model, idb, steps, opts) ->
    deferred = Q.defer()

    results = []
    leadStep = steps[0]
    leadLookup = leadStep.args.shift()

    req = idb.transaction(model.name).objectStore(model.name)
             .index(leadLookup.getIDBIndex())
             .openCursor(leadLookup.getIDBKeyRange())
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        if matchSteps(steps, cursor.value)
          results.push cursor.value
          cursor.continue()
        else
          cursor.continue()
      else
        deferred.resolve results
    req.onerror = (e) ->
      deferred.reject e
    deferred.promise


  __get: (model, idb, lookups, opts) ->
    ## Returns first match found as per the lookup criteria,
    ## or null if not found.
    deferred = Q.defer()

    leadLookup = lookups.shift()

    req = idb.transaction(model.name).objectStore(model.name)
             .index(leadLookup.getIDBIndex())
             .openCursor(leadLookup.getIDBKeyRange())
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        if matchLookups(lookups, cursor.value)
          deferred.resolve cursor.value
        else
          cursor.continue()
      else
        deferred.resolve null
    req.onerror = (e) ->
    deferred.promise


  __getAll: (model, idb, opts) ->
    deferred = Q.defer()

    results = []
    req = idb.transaction(model.name).objectStore(model.name)
              .openCursor()
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        results.push cursor.value
        cursor.continue()
      else
        deferred.resolve results
    req.onerror = (e) ->
      deferred.reject e

    deferred.promise


  collectManyToManyRelations: (relation, startPk) ->
    deferred = Q.defer()
    fetches = []
    @promise.then (idb) ->
      req = idb.transaction(relation.relationTableName)
               .objectStore(relation.relationTableName)
               .index(relation.ownModel + '_' + relation.ownKey)
               .openCursor(IDBKeyRange.only(startPk))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          rel = cursor.value
          objKey = rel["#{relation.relatedModel}_#{relation.relatedKey}"]
          query = {}
          query[relation.model.primaryKey] = objKey
          fetches.push relation.model.query().get(query).evaluate({depth: 0})
          cursor.continue()
        else
          fetchStack = Q.all fetches
          fetchStack.then (objs) ->
            deferred.resolve objs
      req.onerror = (e) ->
        console.error e
    deferred.promise


  clearManyToManyRelations: (relationTable, objID, objField) ->
    deferred = Q.defer()
    @promise.then (idb) ->
      deletes = []
      req = idb.transaction(relationTable, READ_WRITE_FLAG)
               .objectStore(relationTable)
               .index(objField)
               .openCursor(IDBKeyRange.only(objID))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          deletes.push __deleteCursorPromise cursor
          cursor.continue()
        else
          dp = Q.all deletes
          dp.then ->
            deferred.resolve true
      req.onerror = (e) ->
        console.error e
        deferred.reject e
    deferred.promise


  __makeManyToManyRelation: (idb, relationTable, relation) ->
    deferred = Q.defer()
    string = JSON.stringify relation
    relation.id =  CryptoJS.SHA1(string + new Date().getTime()).toString()

    req = idb.transaction(relationTable, READ_WRITE_FLAG)
             .objectStore(relationTable).add(relation)
    req.onsuccess = (e) ->
      deferred.resolve relation
    deferred.promise


  createManyToManyRelations: (relationTable, relations) ->
    deferred = Q.defer()
    @promise.then (idb) =>
      saves = []
      for relation in relations
        saves.push @__makeManyToManyRelation(idb, relationTable, relation)
      sp = Q.all saves
      sp.then (result) =>
        deferred.resolve true
    deferred.promise




  updateForeignKey: (model, objID, fieldName, reverseID) ->
    deferred = Q.defer()
    @promise.then (idb) ->
      req = idb.transaction(model.name, READ_WRITE_FLAG)
               .objectStore(model.name)
               .openCursor(IDBKeyRange.only(objID))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          obj = cursor.value
          obj[fieldName] = reverseID
          ureq = cursor.update obj
          ureq.onsuccess = (e) ->
            deferred.resolve obj
        else
          console.error "Foreign Key Update Failed"
          deferred.reject false
    deferred.promise


  delete: (model, id, opts) ->
    deferred = Q.defer()
    @promise.then (idb) =>
      req = idb.transaction(model.name, READ_WRITE_FLAG)
               .objectStore(model.name)
               .openCursor(IDBKeyRange.only(id))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          ureq = cursor.delete()
          ureq.onsuccess = (e) ->
            deferred.resolve true
          ureq.onerror = (e) ->
            deferred.reject e
        else
          deferred.resolve true
      req.onerror = (e) ->
        deferred.reject e
    deferred.promise



  save: (model, saveObj, opts) ->
    deferred = Q.defer()


    @promise.then (idb) ->
      preSave = Q.defer()

      saveMethod = undefined

      if saveObj[model.primaryKey] == undefined
        preSave.resolve "add"
      else
        curreq = idb.transaction(model.name).objectStore(model.name)
                    .openCursor(IDBKeyRange.only(saveObj[model.primaryKey]))
        curreq.onsuccess = (e) ->
          cursor = e.target.result
          if cursor
            preSave.resolve "update"
          else
            preSave.resolve "add"
        curreq.onerror = (e) ->
          throw e


      preSave.promise.then (method) ->
        if method == "add"
          req = idb.transaction(model.name, READ_WRITE_FLAG)
                   .objectStore(model.name).add(saveObj)
          req.onsuccess = (e) ->
            deferred.resolve saveObj
          req.onerror = (e) ->
            deferred.reject e
        if method == "update"
          req = idb.transaction(model.name, READ_WRITE_FLAG)
                   .objectStore(model.name)
                   .openCursor(IDBKeyRange.only(saveObj[model.primaryKey]))
          req.onsuccess = (e) ->
            cursor = e.target.result
            ureq = cursor.update saveObj
            ureq.onsuccess = (e) ->
              deferred.resolve saveObj

    deferred.promise


JohoDB.Field::makeIDBIndex = (store) ->
  opts = {}
  if @primaryKey then opts.unique = true else opts.unique = false
  if @unique then opts.unique = true
  store.createIndex @name, @name, opts


JohoDB.ManyToManyField::makeIDBIndex = (store) -> return null


matchSteps = (steps, obj) ->
  wasFilteredIn = false
  for step in steps
    if step.action == 'exclude'
      if matchLookups(step.args, obj)
        return false
    if step.action == 'filter'
      if matchLookups(step.args, obj)
        wasFilteredIn = true
  return wasFilteredIn
      

matchLookups = (lookups, obj) ->
  if lookups.length == 0
    return true
  for lookup in lookups
    if lookup.testIDBValue(obj)
      return true
  return false

JohoDB.FieldLookup::testIDBValue = (obj) ->
  value = obj[@field]

  if @type == "exact"
    if value == @value
      return true
  if @type == "gt"
    if value > @value
      return true
  if @type == "gte"
    if value >= @value
      return true
  if @type == "lt"
    if value < @value
      return true
  if @type == "lte"
    if value <= @value
      return true
  return false

JohoDB.FieldLookup::getIDBIndex = ->
  return @field

JohoDB.FieldLookup::getIDBKeyRange = ->
  if @type == "exact"
    return IDBKeyRange.only @value 
  if @type == "gt"
    return IDBKeyRange.lowerBound @value, true
  if @type == "gte"
    return IDBKeyRange.lowerBound @value
  if @type == "lt"
    return IDBKeyRange.upperBound @value, true
  if @type == "lte"
    return IDBKeyRange.upperBound @value
  console.error "No valid lookup found for #{ @type }"
  throw "No valid lookup found for #{ @type }"


stSuc = (log) -> console.log log
stErr = (error) ->
  console.error error
  return true

class window.JohoDB.WebSqlStorage extends JohoDB.BaseStorage
  initDB: (jsSchema, initOpts) ->
    dbDefer = Q.defer()
    @promise = dbDefer.promise
    @p = dbDefer.promise

    db = openDatabase @name, @version, 'JohoDB Database', 2 * 1024 * 1024

    clobber = Q.defer()
    if initOpts.clobber
      console.warn 'Clobbering Database'
      db.transaction (tx) =>
        @__dropAllTables tx, jsSchema
      , (tx, error) =>
        console.error "Failed Clobbering Database"
        clobber.reject error
      , (tx, success) =>
        clobber.resolve true
    else
      clobber.resolve true

    clobber.promise.then =>
      console.log 'Building Tables'
      db.transaction (tx) =>
        @__buildTables tx, jsSchema, initOpts
      , (tx, error) =>
        console.error "Failed building Database", tx, error
      , (tx, success) =>
        dbDefer.resolve db
        console.log "Finished Loading", @

  __dropAllTables: (tx, jsSchema) ->
    for modelName, model of jsSchema.models
      tx.executeSql "DROP TABLE IF EXISTS #{ model.name };"
    for relationName, relationTable of jsSchema.relationTables
      tx.executeSql "DROP TABLE IF EXISTS #{ relationTable.name };"

  __buildTables: (tx, jsSchema, initOpts) ->
    for modelName, model of jsSchema.models
      @__buildTable tx, model
    for relationName, relationTable of jsSchema.relationTables
      @__buildTable tx, relationTable

  __buildTable: (tx, table) ->
    fieldStatements = []
    for name, field of table.fields
      statement = field.makeSQLField()
      if statement
        fieldStatements.push statement
    sqlStatement = "CREATE TABLE IF NOT EXISTS #{ table.name } ("
    sqlStatement += fieldStatements.join ','

    sqlStatement += ");"
    tx.executeSql sqlStatement

  evaluateQuery: (model, queryset, opts) ->
    deferred = Q.defer()

    @promise.then (db) =>
      if queryset.hasStep "get"
        lookups = queryset.getStep("get").args
        @__get(model, db, lookups, opts).then (result) ->
          deferred.resolve result
      else if queryset.hasStep "all"
        @__getAll(model, db, opts).then (result) ->
          deferred.resolve result
      else
        @__executeQuery(model, db, queryset.querySteps, opts).then (result) ->
          deferred.resolve result

    deferred.promise


  __toJsObject: (obj) ->
    jsObj = {}
    for own key, val of obj
      jsObj[key] = val
    return jsObj


  __executeQuery: (model, db, steps, opts) ->
    deferred = Q.defer()
    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sqlStatement = "SELECT * FROM #{ model.name } WHERE "
        sqlStatement += stepsToSQL model, steps
        sqlStatement += ";"
        tx.executeSql sqlStatement, null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, error) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results

    deferred.promise




  __get: (model, db, lookups, opts) ->
    deferred = Q.defer()

    result = null
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sqlStatement = "SELECT * FROM #{ model.name } WHERE "
        sqlStatement += lookupsToSQL model, lookups
        sqlStatement += ";"
        tx.executeSql sqlStatement, null
        , (tx, res) =>
          if res.rows.length > 0
            result = @__toJsObject(res.rows.item(0))
        , (tx, res) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve result

    deferred.promise

  __getAll: (model, db, opts) ->
    deferred = Q.defer()

    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        tx.executeSql  "SELECT * FROM #{ model.name };", null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, result) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results


    deferred.promise


  collectManyToManyRelations: (r, startPk) ->
    deferred = Q.defer()
    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sql = "SELECT * FROM #{ r.relatedModel } WHERE "
        sql += "#{ r.relatedKey } IN ("
        sql += "SELECT #{ r.relatedModel }.#{r.relatedKey} FROM "
        sql += "#{ r.relatedModel}, #{ r.relationTableName } WHERE "
        sql += "#{r.relatedModel}.#{ r.relatedKey } = "
        sql += "#{ r.relationTableName }.#{ r.relatedModel }_#{ r.relatedKey } AND "
        sql += "#{ r.relationTableName }.#{ r.ownModel }_#{ r.ownKey } "
        sql += "= '#{ startPk }');"
        tx.executeSql sql, null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, error) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results
    deferred.promise


  
  clearManyToManyRelations: (relationTable, objID, objField) ->
    deferred = Q.defer()
    @promise.then (db) ->
      db.transaction (tx) ->
        sqlStatement = "DELETE FROM #{relationTable} WHERE #{ objField } IN ("
        sqlStatement += "SELECT #{relationTable}.#{objField} "
        sqlStatement += "FROM #{relationTable} WHERE "
        sqlStatement += "#{relationTable}.#{ objField } = ?);"
        tx.executeSql sqlStatement, [objID]
      , (tx, error) =>
        console.error "Failed clearing ManyToManyRelations", tx, error
      , (tx, success) =>
        deferred.resolve objID
    deferred.promise


  __makeManyToManyRelation: (tx, relationTable, relation) ->
    string = JSON.stringify relation
    relation.id =  CryptoJS.SHA1(string + new Date().getTime()).toString()
    fieldList = []
    valueList = []
    shroud = []
    for key, val of relation
      fieldList.push key
      valueList.push val
      shroud.push "?"

    sqlStatement = "INSERT INTO #{relationTable} ("
    sqlStatement += fieldList.join ','
    sqlStatement += ") VALUES ("
    sqlStatement += shroud.join ','
    sqlStatement += ");"
    tx.executeSql sqlStatement, valueList



  createManyToManyRelations: (relationTable, relations) ->
    deferred = Q.defer()
    @promise.then (db) =>
      db.transaction (tx) =>
        for relation in relations
          @__makeManyToManyRelation tx, relationTable, relation
      , (tx, error) =>
        console.error "Failed m2m update", tx, error
      , (tx, success) =>
        deferred.resolve true
    deferred.promise



  updateForeignKey: (model, objID, fieldName, reverseID) ->
    deferred = Q.defer()
    @promise.then (db) ->
      db.transaction (tx) ->
        sqlStatement = "UPDATE #{ model.name } SET #{ fieldName } = ? "
        sqlStatement += "WHERE #{ model.primaryKey } = ?;"
        tx.executeSql sqlStatement, [reverseID, objID]
      , (tx, error) =>
        console.error "Failed key update", tx, error
      , (tx, success) =>
        deferred.resolve objID
    deferred.promise


  delete: (model, id, opts) ->
    deferred = Q.defer()
    @promise.then (db) =>
      db.transaction (tx) =>
        sql = "DELETE FROM #{ model.name } WHERE #{ model.primaryKey } = ?"
        tx.executeSql sql, [id]
      , (tx, error) =>
        console.error "Failed delete.", tx, error
      , (tx, success) =>
        deferred.resolve true
    deferred.promise


  save: (model, saveObj, opts) ->
    deferred = Q.defer()

    for key, val of saveObj
      if val == undefined then saveObj[key] = ""
    
    @promise.then (db) ->
      db.transaction (tx) ->
        fieldList = []
        valueList = []
        shroud = []
        for key, val of saveObj
          fieldList.push key
          valueList.push val
          shroud.push "?"
        sqlStatement = "INSERT OR REPLACE INTO #{ model.name } ("
        sqlStatement += fieldList.join ','
        sqlStatement += ") VALUES ("
        sqlStatement += shroud.join ","
        sqlStatement += ")"
        tx.executeSql sqlStatement, valueList
      , (tx, error) =>
        console.error "Failed save", tx, error
      , (tx, success) =>
        deferred.resolve saveObj
    deferred.promise

JohoDB.Field::addSQLConstraints = () ->
  if @primaryKey
    return " PRIMARY KEY"
  else if @unique
    return " UNIQUE"
  return ""

JohoDB.Field::makeSQLField = -> return ''

JohoDB.IntField::makeSQLField = ->
  return "#{ @name } INTEGER#{ @addSQLConstraints() }" 

JohoDB.TextField::makeSQLField = ->
  return "#{ @name } TEXT#{ @addSQLConstraints() }" 

JohoDB.DateTimeField::makeSQLField = ->
  return "#{ @name } DATETIME#{ @addSQLConstraints() }" 

JohoDB.ForeignKeyField::makeSQLField = -> 
  return "#{ @name } TEXT#{ @addSQLConstraints() }" 

JohoDB.ManyToManyField::makeSQLField = -> return ''

stepsToSQL = (model, steps) ->
  groupClauses = []
  for step in steps
    if step.action == 'exclude'
      groupClauses.push excludesToSQL(model, step.args)
    if step.action == 'filter'
      groupClauses.push lookupsToSQL(model, step.args)
  return groupClauses.join " AND "
  

lookupsToSQL = (model, lookups) ->
  clauses = []
  clauses.push("#{model.name}.#{lookup.getSQLWhereClause()}") for lookup in lookups
  return clauses.join " AND "

excludesToSQL = (model, lookups) ->
  clauses = []
  clauses.push("NOT #{model.name}.#{lookup.getSQLWhereClause()}") for lookup in lookups
  return clauses.join " AND "


JohoDB.FieldLookup::getSQLWhereClause = ->
  operator = switch @type
    when "exact" then "="
    when "gt" then ">"
    when "gte" then ">="
    when "lt" then "<"
    when "lte" then "=<"
    else console.error "No valid lookup found for #{ @type }"
  return "#{ @field } #{ operator } \"#{ @value}\""
