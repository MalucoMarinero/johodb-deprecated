'use strict'
fs = require 'fs'
path = require 'path'
mm = require 'minimatch'
wrench = require 'wrench'
{spawn} = require 'child_process'


lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet

mountFolder = (connect, dir) ->
  connect.static require('path').resolve(dir)





module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig {
    pkg: grunt.file.readJSON('package.json')
    compass:
      testPage:
        options:
          sassDir: 'src/testPage/sass'
          cssDir: 'testPage/css'
      homePage:
        options:
          sassDir: 'src/homePage/sass'
          cssDir: 'homePage/css'
          environment: "development"
          config: 'config.rb'
    coffee:
      build:
        options:
          join: true
          sourceMap: true
        files:
          'build/<%= pkg.name %>.js': [
            'src/johodb/main.coffee'
            'src/johodb/*.coffee'
            'src/johodb/storageTypes/base.coffee'
            'src/johodb/storageTypes/*.coffee'
          ]
          'testPage/<%= pkg.name %>.js': [
            'src/johodb/main.coffee'
            'src/johodb/*.coffee'
            'src/johodb/storageTypes/base.coffee'
            'src/johodb/storageTypes/*.coffee'
          ]
          'homePage/components/<%= pkg.name %>.js': [
            'src/johodb/main.coffee'
            'src/johodb/*.coffee'
            'src/johodb/storageTypes/base.coffee'
            'src/johodb/storageTypes/*.coffee'
          ]
      testPage:
        expand: true
        cwd: 'src/testPage/coffee'
        src: ['*.coffee']
        dest: 'testPage/js/'
        ext: '.js'
      homePage:
        expand: true
        cwd: 'src/homePage/coffee'
        src: ['*.coffee']
        dest: 'homePage/js/'
        ext: '.js'
    watch:
      testPage:
        files: [
          'src/johodb/*.coffee',
          'src/johodb/storageTypes/*.coffee',
          'src/testPage/**/*.{haml,coffee,sass}'
          'testPage/**/*.{html,css,js}'
        ]
        tasks: ['reloadDispatcher:testPage']
      homePage:
        files: [
          'src/johodb/*.coffee',
          'src/johodb/storageTypes/*.coffee',
          'src/homePage/**/*.{haml,coffee,sass}'
          'homePage/**/*.{html,css,js}'
        ]
        tasks: ['reloadDispatcher:homePage']
    reloadDispatcher:
      testPage:
        "**/*.haml" : ['hamlpy:testPage']
        "**/*.sass" : ['compass:testPage']
        "**/*.coffee" : ['coffee:build', 'coffee:testPage']
        "**/*.{html,css,js}" : ['livereload']
      homePage:
        "**/*.haml" : ['hamlpy:homePage']
        "**/*.sass" : ['compass:homePage']
        "**/*.coffee" : ['coffee:build', 'coffee:homePage']
        "**/*.{html,css,js}" : ['livereload']
    hamlpy:
      testPage:
        src: 'src/testPage'
        dest: 'testPage'
      homePage:
        src: 'src/homePage'
        dest: 'homePage'
    connect:
      options:
        port: 8085
        hostname: '0.0.0.0'
      testPage:
        options:
          middleware: (connect) ->
            return [
              lrSnippet
              mountFolder connect, 'testPage'
            ]
      homePage:
        options:
          middleware: (connect) ->
            return [
              lrSnippet
              mountFolder connect, 'homePage'
            ]
  }

  grunt.renameTask 'regarde', 'watch'

  grunt.registerMultiTask 'hamlpy', 'Compile HAML Files into HTML', () ->
    options = this.options()
    done = this.async()

    this.files.forEach (f) ->
      console.log "Compiling haml from #{ f.src } to #{ f.dest}"
      dest = f.dest
      dir = path.dirname dest

      f.src.map (srcPath) ->
        files = wrench.readdirSyncRecursive srcPath
        files = files.filter mm.filter "**/*.haml"

        files.forEach (srcRelPath) ->
          destRelPath = srcRelPath.replace '.haml', '.html'
          destRelDir = destRelPath.replace /[^\\\/?%*:|"<>"]+\.html/, ""
          if destRelDir
            if not fs.existsSync "#{ dest }/#{ destRelDir }"
              fs.mkdirSync "#{ dest }/#{ destRelDir }"
          process = spawn 'hamlpy', [
            "#{ srcPath }/#{ srcRelPath }"
            "#{ dest }/#{ destRelPath }"
          ]
          process.stdout.on 'data', (data) -> console.log data.toString()
          process.stderr.on 'data', (data) -> console.error data.toString()
          process.on 'exit', (code) ->
            if code is 0
              console.log "  '#{ srcPath }/#{ srcRelPath }' -> '#{ dest }/#{ destRelPath }'"

  grunt.registerMultiTask 'reloadDispatcher', 'Run tasks based on extensions.', ->
    for pattern, tasks of this.data
      if grunt.regarde.changed.some mm.filter pattern
        grunt.task.run tasks

  grunt.registerTask 'server', ->
    grunt.task.run [
      'connect:testPage'
      'livereload-start'
      'watch:testPage'
    ]

  grunt.registerTask 'homePageServer', ->
    grunt.task.run [
      'connect:homePage'
      'livereload-start'
      'watch:homePage'
    ]
