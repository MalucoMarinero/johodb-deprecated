'use strict'


$(document).ready ->
  nav = $('nav ul.nav')
  
  sections = $('section.demonstration section')

  selectNavLi = (name) ->
    nav.find('li').removeClass('active').removeClass('next') 
    nav.find("[href=##{ name }]").parent().addClass 'active'
    nav.find("[href=##{ name }]").parent().next().addClass 'next'

  selectNavLi "introduction"

  heights = []

  updateScrollHeights = ->
    heights = []
    sections.each (i, elem) ->
      heights.unshift [$(elem).offset().top, elem.id]


  updateLinks = ->
    scroll = $(document).scrollTop() + 25
    for height in heights
      if scroll > height[0]
        selectNavLi height[1]
        break

  updateScrollHeights()
  updateLinks()

  $(document).scroll updateLinks

  $(document).resize updateScrollHeights
