window.db = new JohoDB "testDB", 20

db.addSchema 'Household', {
  id: {type: "string", primaryKey: true, autoHash: true}
  address: {type: "string", required: true}
  suburb: {type: "string", required: true}
  postCode: {type: "string", required: true}
}
  
db.addSchema 'People', {
  id: {type: "string", primaryKey: true, autoHash: true}
  firstName: {type: "string", required: true}
  lastName: {type: "string", required: true}
  dateOfBirth: {type: "datetime", required: true}
  home: {type: "fk", relation: "Household", relatedName: "occupants", onDelete: "cascade"}
  bio: {type: "string"}
  pets: {type: "m2m", relation: "Pets", relatedName: "owners"}
}

db.addSchema 'Pets', {
  id: {type: "string", primaryKey: true, autoHash: true}
  name: {type: "string", required: true}
  age: {type: "int"}
}

db.initDB "indexedDb", clobber: true

saves = [
  ->
    db.models.Household.save {
      address: "8/17 Churchill Ave"
      suburb: "Sandy Bay"
      postCode: "7005"
    }
  (result) ->
    window.house1 = result
    db.models.Household.save {
      address: "9 Monterey Sq"
      suburb: "Kingston"
      postCode: "7532"
    }
  (result) ->
    window.house2 = result
    db.models.Household.save {
      address: "11 Malamar Place"
      suburb: "Glenorchy"
      postCode: "7023"
    }
  (result) ->
    window.house3 = result
    db.models.Pets.save {name: "Barney", age: 13}
  (result) ->
    window.pet1 = result
    db.models.Pets.save {name: "Tiger", age: 4}
  (result) ->
    window.pet2 = result
    db.models.People.save {
      firstName: "James"
      lastName: "Rakich"
      home: house2
      dateOfBirth: new Date(1925, 10, 8)
      pets: [pet1, pet2]
    }, {updateRelationships: true}

  (result) ->
    window.person1 = result
    house2.address = "52 Monterey Sq"
    pet2.age = 6
    db.models.People.save {
      firstName: "Joe"
      lastName: "Bloggs"
      home: house2
      dateOfBirth: new Date(1942, 2, 1)
      pets: [pet2]
      bio: "Top bloke here."
    }, {updateRelatedObjs: true}
  (result, relatedObjs) ->
    window.person2 = result
    db.models.Household.save {
      address: "82 Montes Sq"
      suburb: "Rosny Park"
      postCode: "7832"
      occupants: [person1]
    }, {updateRelationships: true}
  (result, relatedObjs) ->
    window.houseToDel = result
    db.models.People.query().all().evaluate()
  (result) ->
    console.log 'People.query().all()', result
    db.models.Pets.query().all().evaluate()
  (result) ->
    console.log 'Pets.query().all()', result
    db.models.Household.delete(houseToDel)
  (result) ->
    db.models.Pets.query().get({name: "Barney"}).evaluate()
  (result) ->
    console.log 'Pets.query().get({name: "Barney"})', result
    db.models.Household.query().all().evaluate()
  (result) ->
    console.log 'Household.query().all()', result
    db.models.People.query().all().evaluate()
  (result) ->
    console.log 'People.query().all()', result
]

result = Q.resolve(true)
for save in saves
  result = result.then save 
