'use strict'

class window.JohoDB.Model
  constructor: (@name, @rawSchema) ->
    @fields = {}
    @relations = {}
    for own fieldName, fieldTraits of @rawSchema
      @fields[fieldName] = new JohoDB.Field(fieldName, fieldTraits)
    @primaryKey = @getPrimaryKey().name

  getRelations: ->
    relations = []
    for own fieldName, field of @fields
      if field.type.match /(ForeignKey|ManyToMany)/
        relations.push new JohoDB.Relation(@name, field)
    return relations

  getPrimaryKey: ->
    for fieldName, field of @fields
      if field.primaryKey
        return field
    throw "No Primary Key found for #{ @name }"

  validate: (obj) ->
    for own key, field of @fields
      field.validate(obj[key])
    for own key, relation of @relations
      relation.validate(obj[key])

  __reduceObj: (obj) ->
    saveObj = {}
    for own key, __ of @fields
      saveObj[key] = obj[key]
    for own key, relation of @relations
      if relation.type == "ForeignKey"
        saveObj[key] = relation.reduce obj[key]
      if relation.type == "ManyToMany"
        delete saveObj[key]
      if relation.type == "OneToMany"
        delete saveObj[key]
    return saveObj

  __collectRelatedObjectUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      updates[r.relatedModel] = {model: r.model, objs: []}
    for own key, r of @relations
      newObjs = r.collectRelatedObjects obj[key], obj[@primaryKey]
      updates[r.relatedModel].objs = updates[r.relatedModel].objs.concat newObjs
    return updates

  __collectForeignKeyUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      updates[r.name] = {model: r.model, name: r.relatedName, ids: []}
    for own key, r of @relations
      if r.type == "OneToMany"
        updates[r.name].ids = updates[r.name].ids.concat r.collectKeys obj[key]
    return updates

  __collectManyToManyUpdates: (obj) ->
    updates = {}
    for own key, r of @relations
      if r.type == "ManyToMany"
        updates[r.relationTableName] = {name: r.relationTableName, updates: [], ownField: r.ownModel + '_' + r.ownKey}
    for own key, r of @relations
      if r.type == "ManyToMany"
        updates[r.relationTableName].updates = updates[r.relationTableName].updates.concat r.collectKeys(obj[key], obj[@primaryKey])
    return updates

  generatePrimaryKey: (obj) ->
    string = JSON.stringify obj
    return CryptoJS.SHA1(string + new Date().getTime()).toString()

  __updateForeignKeyRelation: (objID, fieldName, reverseID) ->
    deferred = Q.defer()
    p = @store.updateForeignKey(@, objID, fieldName, reverseID)
    p.then (result) ->
      deferred.resolve result
    deferred.promise

  __updateRelations: (extraUpdates, keyUpdates, m2mUpdates, reverseID) ->
    stack = []
    if extraUpdates
      for modelName, updateSet of extraUpdates
        for rel in updateSet.objs
          stack.push updateSet.model.save rel
    if keyUpdates
      for fieldName, updateSet of keyUpdates
        for relID in updateSet.ids
          stack.push updateSet.model.__updateForeignKeyRelation(relID, updateSet.name, reverseID)
    if m2mUpdates
      for tableName, updateSet of m2mUpdates
        stack.push @store.clearManyToManyRelations tableName, reverseID, updateSet.ownField
        stack.push @store.createManyToManyRelations tableName, updateSet.updates
    g = Q.all stack

  __saveRelatedObjects: (extraUpdates) ->
    stack = []
    return g

  
  delete: (obj, opts) ->
    deferred = Q.defer()
    if obj[@primaryKey]
      query = {}
      query[@primaryKey] = obj[@primaryKey]
      @query().get(query).evaluate().then (trueObj) =>
        stack = []
        for own key, r of @relations
          if r.type == "OneToMany"
            if r.onDelete == 'cascade' and trueObj[key] != undefined
              for relObj in trueObj[key]
                stack.push r.model.delete(relObj)
            else if r.onDelete == 'setNull' and trueObj[key] != undefined
              for relObj in trueObj[key]
                relObj[r.relatedName] = undefined
                stack.push r.model.save(relObj)
          if r.type == "ManyToMany"
            stack.push @store.clearManyToManyRelations(r.relationTableName, trueObj[@primaryKey], r.ownModel + '_' + r.ownKey)
        stack.push @store.delete(this, trueObj[@primaryKey], {})
        g = Q.all stack
        g.then ->
          deferred.resolve true
    else
      console.error "Object was not provided with a primary key", obj
      deferred.reject "Error"
    deferred.promise


  save: (obj, opts) ->
    deferred = Q.defer()
    extraUpdates = null
    keyUpdates = null
    if @validate obj
      if obj[@primaryKey] == undefined
        obj[@primaryKey] = @generatePrimaryKey obj
      saveObj = @__reduceObj obj
      if opts?.updateRelatedObjs
        extraUpdates = @__collectRelatedObjectUpdates obj
        m2mUpdates = @__collectManyToManyUpdates obj
      if not opts?.updateRelatedObjs and opts?.updateRelationships
        keyUpdates = @__collectForeignKeyUpdates obj
        m2mUpdates = @__collectManyToManyUpdates obj
      p = @store.save(@, saveObj, opts)
      p = p.then (result) =>
        if extraUpdates or keyUpdates or m2mUpdates
          g = @__updateRelations extraUpdates, keyUpdates, m2mUpdates, obj[@primaryKey]
          g.then (relatedObjs) =>
            console.log "Saved #{ @name } with Relations", saveObj, relatedObjs
            deferred.resolve result, relatedObjs
        else if keyUpdates
          g = @__updateForeignKeyRelations keyUpdates, obj[@primaryKey]
          g.then (relatedObjs) =>
            console.log "Saved #{ @name } and updated Keys on Relations", saveObj, relatedObjs
            deferred.resolve result, relatedObjs
        else
          console.log "Saved #{ @name }", saveObj
          deferred.resolve result
      p.fail (e) ->
        deferred.reject e
      p.done()
    deferred.promise

  query: -> new JohoDB.Queryset this


class window.JohoDB.Queryset
  constructor: (@model) ->
    @querySteps = []

  all: ->
    @querySteps.push {
      action: 'all'
      args: {}
    }
    this

  get: (args) ->
    newStep = {
      action: 'get'
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }
      
    if newStep.args.length < 1
      console.error "No arguments provided for get", args
      throw "No arguments provided for get"
    @querySteps.push newStep

    this

  filter: (args) ->
    newStep = {
      action: "filter"
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }

    if newStep.args.length < 1
      console.error "No arguments provided for filter", args
      throw "No arguments provided for filter"

    @querySteps.push newStep
    this

  exclude: (args) ->
    newStep = {
      action: "exclude"
      args: new JohoDB.FieldLookup(k, v) for k, v of args
    }

    if newStep.args.length < 1
      console.error "No arguments provided for filter", args
      throw "No arguments provided for filter"

    @querySteps.push newStep
    this

  hasStep: (stepName) ->
    for step in @querySteps
      if step.action == stepName
        return true
    return false

  getStep: (stepName) ->
    for step in @querySteps
      if step.action == stepName
        return step
    return null

  evaluate: (opts) ->
    if not opts then opts = {}
    if opts.depth == undefined then opts.depth = 1
    deferred = Q.defer()
    p = @model.store.evaluateQuery @model, this, opts
    p = p.then (result) =>
      if opts?.depth
        if Array.isArray result
          stack = []
          for obj in result
            stack.push @__getRelatedObjects(obj, {depth: opts.depth - 1})
          g = Q.all stack
        else
          g = @__getRelatedObjects result, {depth: opts.depth - 1}
        g.then (relatedObjs) =>
          @__zipRelatedObjs result, relatedObjs
          deferred.resolve result
      else
        deferred.resolve result
    p.fail (e) ->
      deferred.reject e
    p.done()
    deferred.promise

  __getRelatedObjects: (obj, opts) ->
    stack = []
    for name, relation of @model.relations
      if relation.type == 'ForeignKey' and obj[name] != undefined
        query = {}
        query[relation.relatedKey] = obj[name]
        stack.push relation.model.query().get(query).evaluate(opts)
      else if relation.type == 'OneToMany'
        query = {}
        query[relation.relatedName] = obj[@model.primaryKey]
        stack.push relation.model.query().filter(query).evaluate(opts)
      else if relation.type == 'ManyToMany'
        stack.push @model.store.collectManyToManyRelations(relation, obj[@model.primaryKey])
      else
        func = -> return []
        stack.push func()
    Q.all stack

  __zipRelatedObjs: (result, relatedObjs) ->
    if Array.isArray result
      i = 0
      for res in result
        @__zipRelatedObj res, relatedObjs[i]
        i += 1
    else
      @__zipRelatedObj result, relatedObjs

  __zipRelatedObj: (obj, relatedObjs) ->
    i = 0
    for name, relation of @model.relations
      obj[name] = relatedObjs[i]
      i += 1






class window.JohoDB.FieldLookup
  constructor: (name, value) ->
    [field, lookupType] = name.split '__'
    @field = field
    @type = lookupType or 'exact'
    @value = value
