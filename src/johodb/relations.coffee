'use strict'

class window.JohoDB.Relation
  constructor: (fromModelName, fromField, skipCheck) ->
    if not skipCheck
      if fromField.type == "ForeignKey"
        return new JohoDB.ForeignKeyRelation fromModelName, fromField, true
      if fromField.type == "ManyToMany"
        return new JohoDB.ManyToManyRelation fromModelName, fromField, true
    @type = fromField.type
    @from =
      modelName: fromModelName
      fieldName: fromField.name
    @to =
      modelName: fromField.relation
      fieldName: fromField.relatedName
    if fromField.onDelete
      @onDelete = fromField.onDelete
    else
      @onDelete = "cascade"

  buildRelationManagers: -> throw "buildRelationManagers Not Implemented"
  buildRelationTable: -> throw "buildRelationTable Not Implemented"
  getTableName: -> throw "getTableName Not Implemented"

class window.JohoDB.ForeignKeyRelation extends JohoDB.Relation
  requiresRelationTable: false

  buildRelationManagers: ->
    fromManager = new JohoDB.ForeignKeyRelationManager(
      @from.fieldName, @to.fieldName, null, @from.modelName, @to.modelName, @to.primaryKey, @from.primaryKey, null)
    toManager = new JohoDB.OneToManyRelationManager(
      @to.fieldName, @from.fieldName, null, @to.modelName, @from.modelName, @from.primaryKey, @to.primaryKey, @onDelete)

    return [fromManager, toManager]


class window.JohoDB.ManyToManyRelation extends JohoDB.Relation
  requiresRelationTable: true

  buildRelationManagers: ->
    fromManager = new JohoDB.ManyToManyRelationManager(
      @from.fieldName, @to.fieldName, @getTableName(), @from.modelName, @to.modelName, @to.primaryKey, @from.primaryKey, null)
    toManager = new JohoDB.ManyToManyRelationManager(
      @to.fieldName, @from.fieldName, @getTableName(), @to.modelName, @from.modelName, @from.primaryKey, @to.primaryKey, null)

    return [fromManager, toManager]

  getTableName: ->
    return "#{ @from.modelName }_#{ @from.fieldName }__#{ @to.modelName }_#{ @to.fieldName }"
  
  buildRelationTable: ->
    return new JohoDB.RelationTable @getTableName(), @from, @to


class window.JohoDB.RelationTable
  constructor: (@name, from, to) ->
    @fields = {}

    @fields["id"] = new JohoDB.Field "id", type: "string", primaryKey: true, autoHash: true
    @primaryKey = "id"

    fromName = "#{ from.modelName }_#{ from.primaryKey }"
    @fields[fromName] = new JohoDB.Field fromName, type: "int", required: true

    toName = "#{ to.modelName }_#{ to.primaryKey }"
    @fields[toName] = new JohoDB.Field toName, type: "int", required: true



class window.JohoDB.ModelRelationManager
  constructor: (@name, @relatedName, @relationTableName, @ownModel, @relatedModel, @relatedKey, @ownKey, @onDelete) ->

  validate: (value) ->
    console.error "Validate not implemented."

    


class window.JohoDB.ForeignKeyRelationManager extends JohoDB.ModelRelationManager
  type: 'ForeignKey'

  validate: (value) ->
    if typeof value == "object"
      if value[@relatedKey] == undefined
        console.error "No primary key provided for relation #{ @name }", value
        throw "Validation Eror"
    if Array.isArray value
      console.error "Foreign Key #{ @name } doesn't support arrays.", value
      throw "Validation Eror"

  reduce: (value) ->
    if typeof value == "object"
      return value[@relatedKey]
    else
      return value

  collectRelatedObjects: (value, reversePk) ->
    if typeof value == "object"
      return [value]
    else
      return []

class window.JohoDB.OneToManyRelationManager extends JohoDB.ModelRelationManager
  type: 'OneToMany'

  validate: (value) ->
    if Array.isArray value
      for rel in value
        if typeof value == "object"
          if rel[@relatedKey] == undefined
            console.error "No primary key provided for relation #{ @name }", value
            throw "Validation Eror"

  collectRelatedObjects: (value, reversePk) ->
    if Array.isArray value
      for obj in value
        obj[@relatedName] = reversePk
      return value
    else
      return []

  collectKeys: (value) ->
    keys = []
    if Array.isArray value
      for obj in value
        keys.push obj[@model.primaryKey]
    return keys

class window.JohoDB.ManyToManyRelationManager extends JohoDB.ModelRelationManager
  type: 'ManyToMany'

  validate: (value) ->
    if Array.isArray value
      for rel in value
        if typeof value == "object"
          if rel[@relatedKey] == undefined
            console.error "No primary key provided for relation #{ @name }", value
            throw "Validation Eror"

  collectRelatedObjects: (value, reversePk) ->
    if Array.isArray value
      return value
    else
      return []

  collectKeys: (value, reversePk) ->
    rels = []
    if Array.isArray value
      for obj in value
        relObj = {}
        relObj[@ownModel + '_' + @ownKey] = reversePk
        relObj[@relatedModel + '_' + @relatedKey] = obj[@model.primaryKey]
        rels.push relObj
    return rels


