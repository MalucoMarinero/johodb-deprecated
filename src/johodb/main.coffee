'use strict'

class window.JohoDB
  constructor: (@name, @version) ->
    @rawSchema = {}

  addSchema: (schemaName, schema) ->
    @rawSchema[schemaName] = schema

  initDB: (storageType, opts) ->
    @__buildJSSchema()
    if storageType == "indexedDb"
      @store = new JohoDB.IndexedDbStorage @name, @version
      model.store = @store for key, model of @models
    else if storageType == "webSql"
      @store = new JohoDB.WebSqlStorage @name, @version
      model.store = @store for key, model of @models

    @store.initDB {models: @models, relationTables: @relationTables}, opts

  __buildJSSchema: ->
    @models = {}
    for own modelName, modelSchema of @rawSchema
      @models[modelName] = new JohoDB.Model modelName, modelSchema
    @__setupRelations()

  __setupRelations: ->
    @relationTables = {}
    @relations = []
    for own modelName, model of @models
      @relations = @relations.concat model.getRelations()
    @__linkPrimaryKeys()
    @__formRelations()

  __linkPrimaryKeys: ->
    for r in @relations
      r.from.primaryKey = @models[r.from.modelName].primaryKey
      r.to.primaryKey = @models[r.to.modelName].primaryKey

  __formRelations: ->
    for r in @relations
      if r.requiresRelationTable
        @relationTables[r.getTableName()] = r.buildRelationTable()
      [from, to] = r.buildRelationManagers() 
      @models[from.ownModel].relations[from.name] = from
      @models[from.ownModel].relations[from.name].model = @models[to.ownModel]
      @models[to.ownModel].relations[to.name] = to
      @models[to.ownModel].relations[to.name].model = @models[from.ownModel]
      

    
  

