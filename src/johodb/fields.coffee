class window.JohoDB.Field
  constructor: (@name, traits, skipCheck) ->
    if not skipCheck
      if traits.type.match /(int|integer)/
        traits.type = "Int"
        return new JohoDB.IntField(@name, traits, true)
      if traits.type.match /(str|string|text)/
        traits.type = "String"
        return new JohoDB.TextField(@name, traits, true)
      if traits.type.match /(datetime)/
        traits.type = "DateTime"
        return new JohoDB.DateTimeField(@name, traits, true)
      if traits.type.match /(fk|foreignKey|foreignkey)/
        traits.type = "ForeignKey"
        return new JohoDB.ForeignKeyField(@name, traits, true)
      if traits.type.match /(m2m|manytomany|manyToMany)/
        traits.type = "ManyToMany"
        return new JohoDB.ManyToManyField(@name, traits, true)
    for own key, value of traits
      @[key] = value

  defaultValidators: []
  validate: (value) ->
    if @required
      if value == undefined
        console.error "Validation Error for #{ @name }, required."
    if @primaryKey
      if not @autoIncrement and not @autoHash
        if value == undefined
          console.error "Validation Error for #{ @name }, required primary key / no auto."
    for validator in @defaultValidators
      if not validator(value)
        console.error "Validation Error for #{ @name }"
    if @validators
      for validator in @validators
        if not validator(value)
          console.error "Validation Error for #{ @name }"
    return true
    

class window.JohoDB.IntField extends JohoDB.Field
  defaultValidators: [
    (value) ->
      number = parseInt value
      if number == NaN
        return false
      return true
  ]

class window.JohoDB.TextField extends JohoDB.Field

class window.JohoDB.DateTimeField extends JohoDB.Field

class window.JohoDB.RelationField extends JohoDB.Field

class window.JohoDB.ForeignKeyField extends JohoDB.RelationField
  validate: -> true

class window.JohoDB.ManyToManyField extends JohoDB.RelationField
  validate: -> true
