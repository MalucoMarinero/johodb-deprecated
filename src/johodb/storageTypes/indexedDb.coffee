if window.webkitIndexedDB
  window.indexedDB = window.webkitIndexedDB
  window.IDBKeyRange = window.webkitIDBKeyRange
  window.IDBTransaction = window.webkitIDBTransaction

READ_WRITE_FLAG = "readwrite"


__deleteCursorPromise = (cursor) ->
  deferred = Q.defer()
  dreq = cursor.delete()
  dreq.onsuccess = (e) ->
    deferred.resolve true
  deferred.promise


class window.JohoDB.IndexedDbStorage extends JohoDB.BaseStorage
  initDB: (jsSchema, initOpts) ->
    dbDefer = Q.defer()
    @promise = dbDefer.promise
    @p = dbDefer.promise

    clobber = Q.defer()
    if initOpts.clobber
      req = window.indexedDB.deleteDatabase @name
      req.onsuccess = (e) =>
        clobber.resolve true
        console.warn "Clobbered Database #{ @name }"
      req.onerror = (e) ->
        clobber.reject e
        throw e
    else
      clobber.resolve true
    clobber.promise.then (result) => @__startDB jsSchema, initOpts, dbDefer

  __startDB: (jsSchema, initOpts, dbDefer) ->

    req = window.indexedDB.open @name, @version
    req.onsuccess = (e) =>
      idb = e.target.result
      console.log 'Finished loading', @
      dbDefer.resolve idb

    req.onupgradeneeded = (e) =>
      idb = e.target.result
      console.log 'Upgrading'
      @__buildTables idb, jsSchema, initOpts, e
    req.onerror = (e) ->
      dbDefer.reject e
      throw e


  __buildTables: (idb, jsSchema, initOpts, e) ->
    for modelName, model of jsSchema.models
      @__buildTable idb, model
    for relationName, relationTable of jsSchema.relationTables
      @__buildTable idb, relationTable


  __buildTable: (idb, model) ->
    store = idb.createObjectStore model.name, {keyPath: model.primaryKey, autoIncrement: true}
    field.makeIDBIndex(store) for name, field of model.fields


  evaluateQuery: (model, queryset, opts) ->
    deferred = Q.defer()

    @promise.then (idb) =>
      if queryset.hasStep "get"
        lookups = queryset.getStep("get").args
        @__get(model, idb, lookups, opts).then (result) ->
          deferred.resolve result
      else if queryset.hasStep "all"
        @__getAll(model, idb, opts).then (result) ->
          deferred.resolve result
      else
        @__executeQuery(model, idb, queryset.querySteps, opts).then (result) ->
          deferred.resolve result

    deferred.promise
    



  __executeQuery: (model, idb, steps, opts) ->
    deferred = Q.defer()

    results = []
    leadStep = steps[0]
    leadLookup = leadStep.args.shift()

    req = idb.transaction(model.name).objectStore(model.name)
             .index(leadLookup.getIDBIndex())
             .openCursor(leadLookup.getIDBKeyRange())
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        if matchSteps(steps, cursor.value)
          results.push cursor.value
          cursor.continue()
        else
          cursor.continue()
      else
        deferred.resolve results
    req.onerror = (e) ->
      deferred.reject e
    deferred.promise


  __get: (model, idb, lookups, opts) ->
    ## Returns first match found as per the lookup criteria,
    ## or null if not found.
    deferred = Q.defer()

    leadLookup = lookups.shift()

    req = idb.transaction(model.name).objectStore(model.name)
             .index(leadLookup.getIDBIndex())
             .openCursor(leadLookup.getIDBKeyRange())
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        if matchLookups(lookups, cursor.value)
          deferred.resolve cursor.value
        else
          cursor.continue()
      else
        deferred.resolve null
    req.onerror = (e) ->
    deferred.promise


  __getAll: (model, idb, opts) ->
    deferred = Q.defer()

    results = []
    req = idb.transaction(model.name).objectStore(model.name)
              .openCursor()
    req.onsuccess = (e) ->
      cursor = e.target.result
      if cursor
        results.push cursor.value
        cursor.continue()
      else
        deferred.resolve results
    req.onerror = (e) ->
      deferred.reject e

    deferred.promise


  collectManyToManyRelations: (relation, startPk) ->
    deferred = Q.defer()
    fetches = []
    @promise.then (idb) ->
      req = idb.transaction(relation.relationTableName)
               .objectStore(relation.relationTableName)
               .index(relation.ownModel + '_' + relation.ownKey)
               .openCursor(IDBKeyRange.only(startPk))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          rel = cursor.value
          objKey = rel["#{relation.relatedModel}_#{relation.relatedKey}"]
          query = {}
          query[relation.model.primaryKey] = objKey
          fetches.push relation.model.query().get(query).evaluate({depth: 0})
          cursor.continue()
        else
          fetchStack = Q.all fetches
          fetchStack.then (objs) ->
            deferred.resolve objs
      req.onerror = (e) ->
        console.error e
    deferred.promise


  clearManyToManyRelations: (relationTable, objID, objField) ->
    deferred = Q.defer()
    @promise.then (idb) ->
      deletes = []
      req = idb.transaction(relationTable, READ_WRITE_FLAG)
               .objectStore(relationTable)
               .index(objField)
               .openCursor(IDBKeyRange.only(objID))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          deletes.push __deleteCursorPromise cursor
          cursor.continue()
        else
          dp = Q.all deletes
          dp.then ->
            deferred.resolve true
      req.onerror = (e) ->
        console.error e
        deferred.reject e
    deferred.promise


  __makeManyToManyRelation: (idb, relationTable, relation) ->
    deferred = Q.defer()
    string = JSON.stringify relation
    relation.id =  CryptoJS.SHA1(string + new Date().getTime()).toString()

    req = idb.transaction(relationTable, READ_WRITE_FLAG)
             .objectStore(relationTable).add(relation)
    req.onsuccess = (e) ->
      deferred.resolve relation
    deferred.promise


  createManyToManyRelations: (relationTable, relations) ->
    deferred = Q.defer()
    @promise.then (idb) =>
      saves = []
      for relation in relations
        saves.push @__makeManyToManyRelation(idb, relationTable, relation)
      sp = Q.all saves
      sp.then (result) =>
        deferred.resolve true
    deferred.promise




  updateForeignKey: (model, objID, fieldName, reverseID) ->
    deferred = Q.defer()
    @promise.then (idb) ->
      req = idb.transaction(model.name, READ_WRITE_FLAG)
               .objectStore(model.name)
               .openCursor(IDBKeyRange.only(objID))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          obj = cursor.value
          obj[fieldName] = reverseID
          ureq = cursor.update obj
          ureq.onsuccess = (e) ->
            deferred.resolve obj
        else
          console.error "Foreign Key Update Failed"
          deferred.reject false
    deferred.promise


  delete: (model, id, opts) ->
    deferred = Q.defer()
    @promise.then (idb) =>
      req = idb.transaction(model.name, READ_WRITE_FLAG)
               .objectStore(model.name)
               .openCursor(IDBKeyRange.only(id))
      req.onsuccess = (e) ->
        cursor = e.target.result
        if cursor
          ureq = cursor.delete()
          ureq.onsuccess = (e) ->
            deferred.resolve true
          ureq.onerror = (e) ->
            deferred.reject e
        else
          deferred.resolve true
      req.onerror = (e) ->
        deferred.reject e
    deferred.promise



  save: (model, saveObj, opts) ->
    deferred = Q.defer()


    @promise.then (idb) ->
      preSave = Q.defer()

      saveMethod = undefined

      if saveObj[model.primaryKey] == undefined
        preSave.resolve "add"
      else
        curreq = idb.transaction(model.name).objectStore(model.name)
                    .openCursor(IDBKeyRange.only(saveObj[model.primaryKey]))
        curreq.onsuccess = (e) ->
          cursor = e.target.result
          if cursor
            preSave.resolve "update"
          else
            preSave.resolve "add"
        curreq.onerror = (e) ->
          throw e


      preSave.promise.then (method) ->
        if method == "add"
          req = idb.transaction(model.name, READ_WRITE_FLAG)
                   .objectStore(model.name).add(saveObj)
          req.onsuccess = (e) ->
            deferred.resolve saveObj
          req.onerror = (e) ->
            deferred.reject e
        if method == "update"
          req = idb.transaction(model.name, READ_WRITE_FLAG)
                   .objectStore(model.name)
                   .openCursor(IDBKeyRange.only(saveObj[model.primaryKey]))
          req.onsuccess = (e) ->
            cursor = e.target.result
            ureq = cursor.update saveObj
            ureq.onsuccess = (e) ->
              deferred.resolve saveObj

    deferred.promise


JohoDB.Field::makeIDBIndex = (store) ->
  opts = {}
  if @primaryKey then opts.unique = true else opts.unique = false
  if @unique then opts.unique = true
  store.createIndex @name, @name, opts


JohoDB.ManyToManyField::makeIDBIndex = (store) -> return null


matchSteps = (steps, obj) ->
  wasFilteredIn = false
  for step in steps
    if step.action == 'exclude'
      if matchLookups(step.args, obj)
        return false
    if step.action == 'filter'
      if matchLookups(step.args, obj)
        wasFilteredIn = true
  return wasFilteredIn
      

matchLookups = (lookups, obj) ->
  if lookups.length == 0
    return true
  for lookup in lookups
    if lookup.testIDBValue(obj)
      return true
  return false

JohoDB.FieldLookup::testIDBValue = (obj) ->
  value = obj[@field]

  if @type == "exact"
    if value == @value
      return true
  if @type == "gt"
    if value > @value
      return true
  if @type == "gte"
    if value >= @value
      return true
  if @type == "lt"
    if value < @value
      return true
  if @type == "lte"
    if value <= @value
      return true
  return false

JohoDB.FieldLookup::getIDBIndex = ->
  return @field

JohoDB.FieldLookup::getIDBKeyRange = ->
  if @type == "exact"
    return IDBKeyRange.only @value 
  if @type == "gt"
    return IDBKeyRange.lowerBound @value, true
  if @type == "gte"
    return IDBKeyRange.lowerBound @value
  if @type == "lt"
    return IDBKeyRange.upperBound @value, true
  if @type == "lte"
    return IDBKeyRange.upperBound @value
  console.error "No valid lookup found for #{ @type }"
  throw "No valid lookup found for #{ @type }"

