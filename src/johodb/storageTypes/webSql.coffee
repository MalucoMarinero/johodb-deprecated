stSuc = (log) -> console.log log
stErr = (error) ->
  console.error error
  return true

class window.JohoDB.WebSqlStorage extends JohoDB.BaseStorage
  initDB: (jsSchema, initOpts) ->
    dbDefer = Q.defer()
    @promise = dbDefer.promise
    @p = dbDefer.promise

    db = openDatabase @name, @version, 'JohoDB Database', 2 * 1024 * 1024

    clobber = Q.defer()
    if initOpts.clobber
      console.warn 'Clobbering Database'
      db.transaction (tx) =>
        @__dropAllTables tx, jsSchema
      , (tx, error) =>
        console.error "Failed Clobbering Database"
        clobber.reject error
      , (tx, success) =>
        clobber.resolve true
    else
      clobber.resolve true

    clobber.promise.then =>
      console.log 'Building Tables'
      db.transaction (tx) =>
        @__buildTables tx, jsSchema, initOpts
      , (tx, error) =>
        console.error "Failed building Database", tx, error
      , (tx, success) =>
        dbDefer.resolve db
        console.log "Finished Loading", @

  __dropAllTables: (tx, jsSchema) ->
    for modelName, model of jsSchema.models
      tx.executeSql "DROP TABLE IF EXISTS #{ model.name };"
    for relationName, relationTable of jsSchema.relationTables
      tx.executeSql "DROP TABLE IF EXISTS #{ relationTable.name };"

  __buildTables: (tx, jsSchema, initOpts) ->
    for modelName, model of jsSchema.models
      @__buildTable tx, model
    for relationName, relationTable of jsSchema.relationTables
      @__buildTable tx, relationTable

  __buildTable: (tx, table) ->
    fieldStatements = []
    for name, field of table.fields
      statement = field.makeSQLField()
      if statement
        fieldStatements.push statement
    sqlStatement = "CREATE TABLE IF NOT EXISTS #{ table.name } ("
    sqlStatement += fieldStatements.join ','

    sqlStatement += ");"
    tx.executeSql sqlStatement

  evaluateQuery: (model, queryset, opts) ->
    deferred = Q.defer()

    @promise.then (db) =>
      if queryset.hasStep "get"
        lookups = queryset.getStep("get").args
        @__get(model, db, lookups, opts).then (result) ->
          deferred.resolve result
      else if queryset.hasStep "all"
        @__getAll(model, db, opts).then (result) ->
          deferred.resolve result
      else
        @__executeQuery(model, db, queryset.querySteps, opts).then (result) ->
          deferred.resolve result

    deferred.promise


  __toJsObject: (obj) ->
    jsObj = {}
    for own key, val of obj
      jsObj[key] = val
    return jsObj


  __executeQuery: (model, db, steps, opts) ->
    deferred = Q.defer()
    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sqlStatement = "SELECT * FROM #{ model.name } WHERE "
        sqlStatement += stepsToSQL model, steps
        sqlStatement += ";"
        tx.executeSql sqlStatement, null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, error) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results

    deferred.promise




  __get: (model, db, lookups, opts) ->
    deferred = Q.defer()

    result = null
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sqlStatement = "SELECT * FROM #{ model.name } WHERE "
        sqlStatement += lookupsToSQL model, lookups
        sqlStatement += ";"
        tx.executeSql sqlStatement, null
        , (tx, res) =>
          if res.rows.length > 0
            result = @__toJsObject(res.rows.item(0))
        , (tx, res) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve result

    deferred.promise

  __getAll: (model, db, opts) ->
    deferred = Q.defer()

    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        tx.executeSql  "SELECT * FROM #{ model.name };", null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, result) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results


    deferred.promise


  collectManyToManyRelations: (r, startPk) ->
    deferred = Q.defer()
    results = []
    @promise.then (db) =>
      db.readTransaction (tx) =>
        sql = "SELECT * FROM #{ r.relatedModel } WHERE "
        sql += "#{ r.relatedKey } IN ("
        sql += "SELECT #{ r.relatedModel }.#{r.relatedKey} FROM "
        sql += "#{ r.relatedModel}, #{ r.relationTableName } WHERE "
        sql += "#{r.relatedModel}.#{ r.relatedKey } = "
        sql += "#{ r.relationTableName }.#{ r.relatedModel }_#{ r.relatedKey } AND "
        sql += "#{ r.relationTableName }.#{ r.ownModel }_#{ r.ownKey } "
        sql += "= '#{ startPk }');"
        tx.executeSql sql, null
        , (tx, res) =>
          results.push(@__toJsObject(res.rows.item(index))) for index in [0...res.rows.length]
        , (tx, error) ->
          console.error "Failed read", tx, error
      , (tx, error) =>
        console.error "Failed read", tx, error
      , (tx, success) =>
        deferred.resolve results
    deferred.promise


  
  clearManyToManyRelations: (relationTable, objID, objField) ->
    deferred = Q.defer()
    @promise.then (db) ->
      db.transaction (tx) ->
        sqlStatement = "DELETE FROM #{relationTable} WHERE #{ objField } IN ("
        sqlStatement += "SELECT #{relationTable}.#{objField} "
        sqlStatement += "FROM #{relationTable} WHERE "
        sqlStatement += "#{relationTable}.#{ objField } = ?);"
        tx.executeSql sqlStatement, [objID]
      , (tx, error) =>
        console.error "Failed clearing ManyToManyRelations", tx, error
      , (tx, success) =>
        deferred.resolve objID
    deferred.promise


  __makeManyToManyRelation: (tx, relationTable, relation) ->
    string = JSON.stringify relation
    relation.id =  CryptoJS.SHA1(string + new Date().getTime()).toString()
    fieldList = []
    valueList = []
    shroud = []
    for key, val of relation
      fieldList.push key
      valueList.push val
      shroud.push "?"

    sqlStatement = "INSERT INTO #{relationTable} ("
    sqlStatement += fieldList.join ','
    sqlStatement += ") VALUES ("
    sqlStatement += shroud.join ','
    sqlStatement += ");"
    tx.executeSql sqlStatement, valueList



  createManyToManyRelations: (relationTable, relations) ->
    deferred = Q.defer()
    @promise.then (db) =>
      db.transaction (tx) =>
        for relation in relations
          @__makeManyToManyRelation tx, relationTable, relation
      , (tx, error) =>
        console.error "Failed m2m update", tx, error
      , (tx, success) =>
        deferred.resolve true
    deferred.promise



  updateForeignKey: (model, objID, fieldName, reverseID) ->
    deferred = Q.defer()
    @promise.then (db) ->
      db.transaction (tx) ->
        sqlStatement = "UPDATE #{ model.name } SET #{ fieldName } = ? "
        sqlStatement += "WHERE #{ model.primaryKey } = ?;"
        tx.executeSql sqlStatement, [reverseID, objID]
      , (tx, error) =>
        console.error "Failed key update", tx, error
      , (tx, success) =>
        deferred.resolve objID
    deferred.promise


  delete: (model, id, opts) ->
    deferred = Q.defer()
    @promise.then (db) =>
      db.transaction (tx) =>
        sql = "DELETE FROM #{ model.name } WHERE #{ model.primaryKey } = ?"
        tx.executeSql sql, [id]
      , (tx, error) =>
        console.error "Failed delete.", tx, error
      , (tx, success) =>
        deferred.resolve true
    deferred.promise


  save: (model, saveObj, opts) ->
    deferred = Q.defer()

    for key, val of saveObj
      if val == undefined then saveObj[key] = ""
    
    @promise.then (db) ->
      db.transaction (tx) ->
        fieldList = []
        valueList = []
        shroud = []
        for key, val of saveObj
          fieldList.push key
          valueList.push val
          shroud.push "?"
        sqlStatement = "INSERT OR REPLACE INTO #{ model.name } ("
        sqlStatement += fieldList.join ','
        sqlStatement += ") VALUES ("
        sqlStatement += shroud.join ","
        sqlStatement += ")"
        tx.executeSql sqlStatement, valueList
      , (tx, error) =>
        console.error "Failed save", tx, error
      , (tx, success) =>
        deferred.resolve saveObj
    deferred.promise

JohoDB.Field::addSQLConstraints = () ->
  if @primaryKey
    return " PRIMARY KEY"
  else if @unique
    return " UNIQUE"
  return ""

JohoDB.Field::makeSQLField = -> return ''

JohoDB.IntField::makeSQLField = ->
  return "#{ @name } INTEGER#{ @addSQLConstraints() }" 

JohoDB.TextField::makeSQLField = ->
  return "#{ @name } TEXT#{ @addSQLConstraints() }" 

JohoDB.DateTimeField::makeSQLField = ->
  return "#{ @name } DATETIME#{ @addSQLConstraints() }" 

JohoDB.ForeignKeyField::makeSQLField = -> 
  return "#{ @name } TEXT#{ @addSQLConstraints() }" 

JohoDB.ManyToManyField::makeSQLField = -> return ''

stepsToSQL = (model, steps) ->
  groupClauses = []
  for step in steps
    if step.action == 'exclude'
      groupClauses.push excludesToSQL(model, step.args)
    if step.action == 'filter'
      groupClauses.push lookupsToSQL(model, step.args)
  return groupClauses.join " AND "
  

lookupsToSQL = (model, lookups) ->
  clauses = []
  clauses.push("#{model.name}.#{lookup.getSQLWhereClause()}") for lookup in lookups
  return clauses.join " AND "

excludesToSQL = (model, lookups) ->
  clauses = []
  clauses.push("NOT #{model.name}.#{lookup.getSQLWhereClause()}") for lookup in lookups
  return clauses.join " AND "


JohoDB.FieldLookup::getSQLWhereClause = ->
  operator = switch @type
    when "exact" then "="
    when "gt" then ">"
    when "gte" then ">="
    when "lt" then "<"
    when "lte" then "=<"
    else console.error "No valid lookup found for #{ @type }"
  return "#{ @field } #{ operator } \"#{ @value}\""
